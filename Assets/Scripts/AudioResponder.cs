﻿using UnityEngine;
using System.Collections;

public class AudioResponder : MonoBehaviour {

	public enum DrawMode
	{
		Texture,
		MeshReactive,
		MeshAdditive,
		MeshReplacing
	}

	public enum MeshType
	{
		Plane,
		Sphere,
		Circle
	}

	public DrawMode drawMode;
	public MeshType meshType;
	public bool smoothFrames;

    public int size;
	public int gridSubdivisions;
    public float scale;
	public float radius;
	public float threshold;
	public float additiveAmount;

	public float maxFrequencyMagnitude = 0;

	float[] samples;

    float[] spectrum;
	float[] spectrumA;
	float[] spectrumB;
	float[] spectrumC;
	int historyPoint;
	bool canSmooth = false;

	float[,] map;

	public AudioSource source;
    public AudioClip clip;
    public float sample;

	public Reactor[] reactors;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        samples = new float[source.clip.samples * source.clip.channels];

        source.clip.GetData(samples, 0);        
		spectrum = spectrumA = spectrumB = spectrumC = new float[size*size];

    }

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();

		map = new float[size, size];

		int x = 0;
		int y = 0;
		for (int i = 0; i < size*size-1d; i++)
		{
			try
			{
				map[x, y] = 0;
			}
			catch (System.IndexOutOfRangeException e)
			{
				Debug.Log("X = " + x + " Y = " + y + " | " + e.Message);
			}
			if(x >= size-1)
			{
				y++;
				x = 0;
			}
			else
			{
				x++;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
        sample = samples[source.timeSamples];

		spectrum = new float[size * size];
		AudioListener.GetSpectrumData (spectrum, 0, FFTWindow.Rectangular);

		if(smoothFrames){
			if(historyPoint == 1){
				spectrum.CopyTo(spectrumA, 0);
				historyPoint = 2;
			}
			if(historyPoint == 2){
				spectrum.CopyTo(spectrumB, 0);
				historyPoint = 3;
			}
			if(historyPoint == 3){
				spectrum.CopyTo(spectrumC, 0);
				historyPoint = 1;
				canSmooth = true;
			}
			if(canSmooth){
				for(int f = 0; f<spectrum.Length; f++){
					spectrum[f] = (spectrumA[f] + spectrumB[f] + spectrumC[f]) /3;
				}
			}
		}

        int length = Mathf.Min(reactors.Length, spectrum.Length - 1);

        
        int x = 0;
		int y = size/2;

        for (int i = 0; i < spectrum.Length-1; i++)
        {
            try
            {
				if(Mathf.Abs(spectrum[i]) > maxFrequencyMagnitude){
					maxFrequencyMagnitude = Mathf.Abs(spectrum[i]);
				}

				if(drawMode == DrawMode.MeshAdditive){
					map[x,y] += additiveAmount;
				}
				else if(drawMode == DrawMode.MeshReplacing){
					if(spectrum[i] > map[x,y]){
						map[x,y] = spectrum[i] * scale;
					}
				}
				else if (drawMode == DrawMode.MeshReactive){
					if(spectrum[i] > threshold){
						map[x,y] = spectrum[i]*scale;
					}
				}

					

            }
            catch (System.IndexOutOfRangeException e)
            {
                Debug.Log("X = " + x + " Y = " + y + " | " + e.Message);
            }
            if(x >= size-1)
            {
				if (y < size / 2) {
					y = y + (size / 2);
				} else{
					y = (y + 1) - (size / 2);
				}

                y++;
				y = Mathf.Clamp (y, 0, size - 1);
                x = 0;
            }
            else
            {
                x++;
            }
        }

        MapDisplay display = FindObjectOfType<MapDisplay>();
		if (drawMode == DrawMode.Texture)
        {
            display.DrawNoiseMap(map);
        }
		else if (drawMode == DrawMode.MeshReactive || drawMode == DrawMode.MeshAdditive || drawMode == DrawMode.MeshReplacing)
        {
			if (meshType == MeshType.Plane) {
				display.DrawMesh (MeshGenerator.GenerateTerrainMesh (map, scale));
			}
			if (meshType == MeshType.Sphere) {
				display.DrawMesh (MeshGenerator.GenerateSphereMesh (map, scale, radius));
			}
			if (meshType == MeshType.Circle) {
				display.DrawMesh (MeshGenerator.GenerateReactiveCircleMesh (map, scale, radius));
			}
        }

		//Update reactors
        
        for(int i = 0; i<length; i++)
        {
            //reactors[i].obj.transform.localScale = new Vector3(0.5f, 50*spectrum[i], 1);
        }
	}
}

[System.Serializable]
public struct Reactor{
	public GameObject obj;
	public int frequency;
	public int bandwidth;
}

