﻿using UnityEngine;
using System.Collections;

public class Gridifier : MonoBehaviour {

	public MeshRenderer meshRenderer;
	public WidthHeight size;
	public int gridScaling;
	public int gridInterval;
	public int gridOffset;
	public Color gridLineColor;
	public Color gridBackColor;
	public bool debugging;

	// Use this for initialization
	void Start () {
		AssignTexture(GenerateGrid(size.width, size.height));
	
	}
	
	// Update is called once per frame
	void Update () {
		if (debugging) {
			AssignTexture(GenerateGrid(size.width, size.height));
		}
		
	}

	public void AssignTexture(Texture2D texture){
		meshRenderer.sharedMaterial.mainTexture = texture;
	}

	public Texture2D GenerateGrid(int width, int height){
		Texture2D texture = new Texture2D (width*gridScaling, height*gridScaling);
		Color[] colorMap = new Color[height * width * gridScaling * gridScaling];
		for (int y = 0; y < height * gridScaling; y++) {
			for (int x = 0; x < width * gridScaling; x++) {
				if ((y % gridInterval == 0) || (x % gridInterval == 0)) {
					colorMap [y * width * gridScaling + x + gridOffset] = gridLineColor;
				} else {
					colorMap [y * width * gridScaling + x + gridOffset] = gridBackColor;
				}
			}
		}

		texture.SetPixels (colorMap);
		texture.Apply ();

		return texture;
	}
}

[System.Serializable]
public struct WidthHeight{
	public int width;
	public int height;
}
