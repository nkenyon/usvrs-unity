﻿using UnityEngine;
using System.Collections;

public static class MeshGenerator{

	public static MeshData GenerateTerrainMesh(float[,] heightMap, float heightMultiplier)
    {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);
        float topLeftX = (width - 1) / -2f;
        float topLeftZ = (height - 1) / 2f;

        MeshData meshData = new MeshData(width, height);
        int vertexIndex = 0;

        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                meshData.vertices[vertexIndex] = new Vector3(topLeftX + x, heightMap[x, y] * heightMultiplier, topLeftZ - y);
                meshData.uvs[vertexIndex] = new Vector2(x / (float)width, y / (float)height);

                if(x < width-1 && y < height - 1)
                {
                    meshData.AddTriangle(vertexIndex, vertexIndex + width + 1, vertexIndex + width);
                    meshData.AddTriangle(vertexIndex + width + 1, vertexIndex, vertexIndex + 1);
                }
                vertexIndex++;
            }
        }

        return meshData;
    }

	public static Mesh GenerateSphereMesh(float[,] heightMap, float heightMultiplier, float radius){
		int width = heightMap.GetLength (0);
		int height = heightMap.GetLength (1);

		float[] newHeightMap = new float[width * height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				newHeightMap [y * width + x] = heightMap [x, y];
			}
		}


		Mesh mesh = new Mesh ();

		// Longitude |||
		int nbLong = 24;
		// Latitude ---
		int nbLat = 16;

		#region Vertices
		Vector3[] vertices = new Vector3[(nbLong+1) * nbLat + 2];
		float _pi = Mathf.PI;
		float _2pi = _pi * 2f;

		vertices[0] = Vector3.up * radius;
		for( int lat = 0; lat < nbLat; lat++ )
		{
			float a1 = _pi * (float)(lat+1) / (nbLat+1);
			float sin1 = Mathf.Sin(a1);
			float cos1 = Mathf.Cos(a1);

			for( int lon = 0; lon <= nbLong; lon++ )
			{
				float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
				float sin2 = Mathf.Sin(a2);
				float cos2 = Mathf.Cos(a2);

				vertices[ lon + lat * (nbLong + 1) + 1] = new Vector3( sin1 * cos2, cos1, sin1 * sin2 ) * radius;
			}
		}
		vertices[vertices.Length-1] = Vector3.up * -radius;


		//Modify based on heightmap and distance to the center;

		int length = Mathf.Min (vertices.Length, newHeightMap.Length);

		Vector3 origin = new Vector3 (0f, 0f, 0f);
		Vector3 pathToCenter = new Vector3 (0f, 0f, 0f);
		Vector3 vertex = new Vector3 (0f, 0f, 0f);

		for(int j = 0; j< length; j++){
			vertex = vertices [j];
			pathToCenter = vertex - origin;
			pathToCenter.Normalize ();
			vertices [j] -= pathToCenter * newHeightMap [j];
		}

		#endregion

		#region Normales		
		Vector3[] normales = new Vector3[vertices.Length];
		for( int n = 0; n < vertices.Length; n++ )
			normales[n] = -vertices[n].normalized;
		#endregion

		#region UVs
		Vector2[] uvs = new Vector2[vertices.Length];
		uvs[0] = Vector2.up;
		uvs[uvs.Length-1] = Vector2.zero;
		for( int lat = 0; lat < nbLat; lat++ )
			for( int lon = 0; lon <= nbLong; lon++ )
				uvs[lon + lat * (nbLong + 1) + 1] = new Vector2( (float)lon / nbLong, 1f - (float)(lat+1) / (nbLat+1) );
		#endregion

		#region Triangles
		int nbFaces = vertices.Length;
		int nbTriangles = nbFaces * 2;
		int nbIndexes = nbTriangles * 3;
		int[] triangles = new int[ nbIndexes ];

		//Top Cap
		int i = 0;
		for( int lon = 0; lon < nbLong; lon++ )
		{
			triangles[i++] = 0;
			triangles[i++] = lon+1;
			triangles[i++] = lon+2;
		}

		//Middle
		for( int lat = 0; lat < nbLat - 1; lat++ )
		{
			for( int lon = 0; lon < nbLong; lon++ )
			{
				int current = lon + lat * (nbLong + 1) + 1;
				int next = current + nbLong + 1;

				triangles[i++] = next + 1;
				triangles[i++] = current + 1;
				triangles[i++] = current;

				triangles[i++] = next;
				triangles[i++] = next + 1;
				triangles[i++] = current;
			}
		}

		//Bottom Cap
		for( int lon = 0; lon < nbLong; lon++ )
		{
			triangles[i++] = vertices.Length - (lon+1) - 1;
			triangles[i++] = vertices.Length - (lon+2) - 1;
			triangles[i++] = vertices.Length - 1;
		}
		#endregion



		mesh.vertices = vertices;
		mesh.normals = normales;
		mesh.uv = uvs;
		mesh.triangles = triangles;

		mesh.RecalculateBounds();
		mesh.Optimize();

		return mesh;

	}

	public static Mesh GenerateReactiveCircleMesh(float[,] heightMap, float heightMultiplier, float radius){
		int width = heightMap.GetLength (0);
		int height = heightMap.GetLength (1);

		float[] newHeightMap = new float[width * height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				newHeightMap [y * width + x] = heightMap [x, y];
			}
		}

		int count = newHeightMap.Length;

		Mesh mesh = new Mesh ();

		Vector3[] vertices = new Vector3[count];
		int[] triangles = new int[(newHeightMap.Length-1)*3];

		vertices [0] = new Vector3(0,0,0);
		int i = 1;
		int t = 0;

		for (float theta = 0; theta < 2 * Mathf.PI; theta += (2* Mathf.PI) / (count-1)) {
			vertices [i] = new Vector3 (Mathf.Cos (theta), newHeightMap[i], Mathf.Sin (theta)) * radius;

			if (i > 1) {
				triangles [t] = i;
				triangles [t + 1] = i - 1;
				triangles [t + 2] = 0;
				t += 3;
			}
			i++;
		}
		triangles [t] = 1;
		triangles [t + 1] = i - 1;
		triangles [t + 2] = 0;

		Vector3[] normales = new Vector3[vertices.Length];
		for (int n = 0; n < vertices.Length; n++) {
			normales [n] = vertices [n].normalized;
		}
		Vector2[] uvs = new Vector2[vertices.Length];

		for (int j=0; j < uvs.Length; j++) {
			uvs[j] = new Vector2(vertices[j].x, vertices[j].z);
		}

		Debug.Log ("vertices size: " + i);
		Debug.Log ("triangles size: " + triangles.Length);
		Debug.Log ("triangles index: " + t);

		mesh.vertices = vertices;
		//mesh.normals = normales;
		mesh.uv = uvs;
		mesh.triangles = triangles;

		mesh.RecalculateBounds();
		mesh.RecalculateNormals ();
		mesh.Optimize();

		return mesh;

	}
}

public class MeshData
{
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    int triangleIndex;

    public MeshData(int meshWidth, int meshHeight)
    {
        vertices = new Vector3[meshWidth * meshHeight];
		uvs = new Vector2[meshWidth * meshHeight];
        triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];
    }

    public void AddTriangle(int a, int b, int c)
    {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = b;
        triangles[triangleIndex + 2] = c;
        triangleIndex += 3;
    }

	public void SetBarycentricUVs(int width, int height){
		
	}

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }

}