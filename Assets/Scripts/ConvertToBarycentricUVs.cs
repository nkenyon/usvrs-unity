﻿using UnityEngine;
using System.Collections;

public class ConvertToBarycentricUVs : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector2[] GetBarycentricFixed(){
		Vector2[] uvs = new Vector2[4];

		const int topLeft = 0;
		const int topRight = 1;
		const int bottomLeft = 2;
		const int bottomRight = 3;

		uvs [topLeft] = new Vector2 (0, 0);
		uvs [topRight] = new Vector2 (0, 0);
		uvs [bottomLeft] = new Vector2 (0, 0);
		uvs [bottomRight] = new Vector2 (0, 0);

		return uvs;
	}
}
