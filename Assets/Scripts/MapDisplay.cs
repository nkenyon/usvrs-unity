﻿using UnityEngine;
using System.Collections;

public class MapDisplay : MonoBehaviour {

    public Renderer textureRenderer;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
	public MeshCollider meshCollider;

	public int gridScaling;
	public int gridInterval;
	public Color gridLineColor;
	public Color gridBackColor;

    public void DrawNoiseMap(float[,] map)
    {
        int width = map.GetLength(0);
        int height = map.GetLength(1);

        Texture2D texture = new Texture2D(width, height);

        Color[] colorMap = new Color[width * height];

        for(int y = 0; y < height; y++){
            for(int x = 0; x <width; x++)
            {
                colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, map[x, y]);
            }
        }

        texture.SetPixels(colorMap);
        texture.Apply();

        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);

    }

    public void DrawNoiseMap(Color[] colorMap, int width, int height)
    {
        Texture2D texture = new Texture2D(width, height);

        texture.SetPixels(colorMap);
        texture.Apply();

        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);

    }

    public void DrawMesh(MeshData meshData)
    {
        meshFilter.sharedMesh = meshData.CreateMesh();
		meshCollider.sharedMesh = meshData.CreateMesh ();
    }

    public void DrawMesh(MeshData meshData, Texture2D texture)
    {
		meshFilter.sharedMesh = meshData.CreateMesh();
		meshCollider.sharedMesh = meshData.CreateMesh ();
		meshRenderer.sharedMaterial.mainTexture = texture;
    }

	public void DrawMesh(Mesh mesh){
		meshFilter.sharedMesh = mesh;
		meshCollider.sharedMesh = mesh;
	}
}
