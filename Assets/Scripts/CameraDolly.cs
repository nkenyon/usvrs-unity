﻿using UnityEngine;
using System.Collections;

public class CameraDolly : MonoBehaviour {
    public Transform start;
    public Transform stop;
    public float speed;

    AudioSource song;

	// Use this for initialization
	void Start () {
        this.transform.position = start.position;
        song = FindObjectOfType<AudioSource>();
        speed = Vector3.Distance(start.position, stop.position) / song.clip.length;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 v = stop.position - this.transform.position;
        v.Normalize();
        v *= speed * Time.deltaTime;

        this.transform.position += v;
	}
}
